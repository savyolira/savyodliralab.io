function snapCrackle(maxValue){
    
    let test = "";
    for(let i = 1; i <= maxValue ; i++){
       
        if(i % 2 !== 0 && i % 5 === 0){
            test = test + "SnapCrackle, ";
        }else if(i % 2 !== 0){
            test = test + "Snap, ";
        }else if(i % 5 === 0){
            test = test + "Crackle, ";
        }else{
            test = test + i + ", ";
        }
        
    }

return test
}
